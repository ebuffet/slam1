drop database if exists TDM1;
create database TDM1;
use TDM1;

create table Tournoi(
	idTournoi int primary key auto_increment,
	nomTournoi varchar(100) not null
) engine InnoDB;

create table Equipe(
	idEquipe int primary key auto_increment,
    nomEquipe varchar(100) not null unique
) engine InnoDB;

create table Participation(
	idEquipe int,
    idTournoi int,
    score int default 0,
    primary key (idEquipe,idTournoi),
    foreign key (idEquipe) references Equipe(idEquipe) on delete cascade on update cascade,
    foreign key (idTournoi) references Tournoi(idTournoi) on delete restrict on update cascade
) engine InnoDB;

insert into Tournoi (nomTournoi) values
	('Open d\'Autralie'),
    ('Roland Garros'),
    ('US Open'),
    ('Wimbledon');

insert into Equipe (nomEquipe) values
	('France'),
    ('Espagne'),
    ('USA');

insert into Participation (idEquipe, idTournoi) values
	(1,1),
    (2,1),
    (3,1);

Select * from participation;
select * from equipe;
select * from Tournoi;
-- check contraintes
insert into Equipe (nomEquipe) values
	('France');

delete from Tournoi where nomTournoi = 'Open d\'Autralie';
delete from Equipe where idEquipe = 1;