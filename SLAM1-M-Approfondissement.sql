drop database if exists TDM1;
create database TDM1;
use TDM1;

create table Personne (
	id SMALLINT auto_increment primary key,
    nom varchar(100) not null,
    prenom varchar(100) not null,
    mail varchar(100) not null,
    pass varchar(100) not null,
    type char(1) check (type in ('u','a','d'))) Engine InnoDB;
    
create table Departement (
	id SMALLINT auto_increment primary key,
    nom_dept varchar(100) not null) engine InnoDB;

create table Projet (
	id SMALLINT auto_increment primary key,
    nom varchar(100) not null,
    departement SMALLINT,
    etat char(1) check (etat in ('a','d')),
    foreign key (departement) references Departement(id) on delete restrict on update cascade) engine InnoDB;

create table Jeton (
	personne SMALLINT primary key,
    jeton char(19) not null,
    foreign key (personne) references Personne(id) on delete cascade on update cascade) engine InnoDB;

create table Affectation (
	personne SMALLINT,
    projet SMALLINT,
    primary key(personne,projet),
    foreign key (personne) references Personne(id) on delete restrict on update cascade,
    foreign key (projet) references Projet(id) on delete restrict on update cascade) engine InnoDB;

create table Travail (
	personne SMALLINT,
    projet SMALLINT,
    temps INT,
    primary key(personne,projet),
    foreign key (personne) references Personne(id) on delete cascade on update cascade,
    foreign key (projet) references Projet(id) on delete cascade on update cascade) engine InnoDB;

create table Chronometre (
	utilisateur SMALLINT,
    nochrono TINYINT check (nochrono >= 1 and nochrono <=8),
    projet SMALLINT,
    date_demarrage datetime,
    primary key(utilisateur,nochrono),
    foreign key (utilisateur) references Personne(id) on delete cascade on update cascade,
    foreign key (projet) references Projet(id) on delete set null on update set null) engine InnoDB;

insert into Personne (nom, prenom, mail, pass, type) values 
	('De Gaulle','Charles','charles.degaulle@elysee.fr','password','u'),
    ('Pompidou','Georges','georges.pompidou@elysee.fr','password','a'),
    ('Giscard d\'Estain','Valérie','valerie.giscarddestain@elysee.fr','password','d');
insert into Departement (nom_dept) values
	('RH'),
    ('Finances'),
    ('IT');
insert into Projet (nom, departement, etat) values
	('projet x','1','d'),
    ('projet y','1','a'),
    ('projet z','2','a');
insert into Affectation values
	('1','1'),
    ('1','2'),
    ('2','3');
insert into Travail values
	('1','1','34534534'),
    ('1','2','345634363'),
    ('2','3',NULL);
insert into Chronometre values
	('1','8','2',now()),
    ('2','5','3',now()),
    ('3','4','3',now());
insert into Jeton values
	('1','SDERFS54RDFSDFRE654'),
	('2','SDERFS54RDFSDFRE654'),
    ('3','SDERFS54RDFSDFRE654');
select * from jeton;
insert into Personne (nom, prenom, mail, pass, type) values 
	('De Gaulle','Charles','charles.degaulle@elysee.fr','password','z');
insert into Chronometre values
	('4','7',NULL,NULL);
